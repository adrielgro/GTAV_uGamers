﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using NativeUI;

namespace BasicResourceClient
{
    class Markers : BaseScript
    {
        private int maxDistanceTextDraw = 50;

        MenuPool menuPool = new MenuPool();

        Menu.Jobs menuJobs = new Menu.Jobs();
        UIMenu mJobs;

        private Vector3[] markers = new[] {
            new Vector3(-124.9463f, 6208.0240f, 30.2048f), // Cartel de empleos
        };
        private String[] texts = new[]
        {
            "ver la lista de empleos", // Cartel de empleos
        };

        private String[] textJobs = new[]
        {
            "Mecánico",
            "Repartidor de correos",
            "Camionero",
            "Granjero",
            "Transportista",
            "Repartidor de comida",
            "Recolector de basura",
            "Taxista"
        };
        private Vector3[] markerJobs = new[] {
            new Vector3(107.0220f, 6612.3560f, 31.9810f), // Mecánico
            new Vector3(-421.1296f, 6136.662f, 30.8773f), // Repartidor de correos
            new Vector3(-17.8192f, 6304.3840f, 30.3749f), // Camionero
            new Vector3(413.0002f, 6539.6220f, 26.7272f), // Granjero
            new Vector3(36.8595f, 6549.8250f, 30.4255f), // Transportista
            new Vector3(-134.8040f, 6378.8970f, 30.7539f), // Repartidor de comida
            new Vector3(-85.9159f, 6493.6570f, 30.4909f), // Recolector de basura
            new Vector3(-38.3101f, 6419.4450f, 31.4904f) // Taxista
        };

        

        public Markers()
        {
            mJobs = menuJobs.menu;
            menuPool.Add(mJobs);

            Tick += OnTick;
        }

        private async Task OnTick()
        {
            Vector3 playerPos = Game.PlayerPed.Position;

            for (int i = 0; i < markers.Length; i++)
            {
                if (API.GetDistanceBetweenCoords(playerPos.X, playerPos.Y, playerPos.Z, markers[i].X, markers[i].Y, markers[i].Z, true) < maxDistanceTextDraw)
                {
                    Function.Call(Hash.DRAW_MARKER, 1, markers[i].X, markers[i].Y, markers[i].Z, 0, 0, 0, 0, 0, 0, 1.5, 1.5, 1.5, 42, 215, 242, 155, 0, 0, 2, 0, 0, 0, 0); // Dibujar marcadores
                    if (API.GetDistanceBetweenCoords(playerPos.X, playerPos.Y, playerPos.Z, markers[i].X, markers[i].Y, markers[i].Z, true) < 1.6)
                    {
                        API.SetTextComponentFormat("STRING");
                        API.AddTextComponentString("Presiona  ~INPUT_VEH_HORN~ para " + texts[i] + ".");
                        API.DisplayHelpTextFromStringLabel(0, false, false, -1);

                        await Task.FromResult(0);
                        menuPool.ProcessMenus();
                        if (Game.IsControlJustReleased(1, (Control)119)) // Tecla E
                        {
                            mJobs.Visible = !mJobs.Visible;
                        }
                    }
                        
                }
            }

            for (int i = 0; i < markerJobs.Length; i++)
            {
                if (API.GetDistanceBetweenCoords(playerPos.X, playerPos.Y, playerPos.Z, markerJobs[i].X, markerJobs[i].Y, markerJobs[i].Z, true) < maxDistanceTextDraw)
                {
                    Function.Call(Hash.DRAW_MARKER, 27, markerJobs[i].X, markerJobs[i].Y, markerJobs[i].Z, 0, 0, 0, 0, 0, 0, 1.5, 1.5, 1.5, 42, 215, 242, 155, 0, 0, 2, 0, 0, 0, 0); // Dibujar marcadores
                    if (API.GetDistanceBetweenCoords(playerPos.X, playerPos.Y, playerPos.Z, markerJobs[i].X, markerJobs[i].Y, markerJobs[i].Z, true) < 1.6)
                    {
                        API.SetTextComponentFormat("STRING");
                        API.AddTextComponentString("Presiona  ~INPUT_VEH_HORN~ para obtener el empleo de " + textJobs[i] + ".");
                        API.DisplayHelpTextFromStringLabel(0, false, false, -1);
                        
                        if (Game.IsControlJustReleased(1, (Control)119)) // Tecla E
                        {
                            // Accion para obtener empleo
                        }
                    }

                }
            }


        }
    }
}
