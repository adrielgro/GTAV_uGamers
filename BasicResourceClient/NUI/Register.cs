﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CitizenFX;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;

namespace BasicResourceClient.NUI
{
    internal static class Register
    {
        internal static int _enabled { get; set; }
        internal static bool _runPattern { get; set; }

        //Sent current resource name to the ui
        internal static void InitData()
        {
            string name = API.GetCurrentResourceName();
            Utils.DebugWriteLine($"Sending Current resouce name {name}");
            API.SendNuiMessage($"{{\"type\":\"initdata\", \"name\":\"{name}\"}}");
        }

        //Enable full ui control and cursor
        internal static void EnableUI()
        {
            Utils.DebugWriteLine("Activando UI de registro...");
            API.SendNuiMessage("{\"type\":\"enableui\", \"enable\":true}");
            API.SetNuiFocus(true, true);
            _enabled = 2;
        }


        //Disable the UI and cursor
        internal static void DisableUI()
        {
            Utils.DebugWriteLine("Desactivando UI de registro..");
            API.SendNuiMessage("{\"type\":\"enableui\", \"enable\":false}");
            API.SetNuiFocus(false, false);
            _enabled = 0;
        }
    
        static Register()
        {
            _enabled = 0;
            _runPattern = false;
        }

        internal static CallbackDelegate FinishUI(IDictionary<string, Object> data, CallbackDelegate cb)
        {
            //Utils.DebugWriteLine("Finish Executed");
            
            DisableUI();
            return cb;
        }
    }
}
