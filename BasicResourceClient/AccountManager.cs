﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;

namespace BasicResourceClient
{
    class AccountManager : BaseScript
    {
        private bool _firstTick = false;
        public static bool _logged = false;

        public AccountManager()
        {
            Tick += OnAutoSaveAccount;
        }

        private async Task OnAutoSaveAccount()
        {
            if (_logged)
            {
                if (_firstTick)
                {
                    Vector3 pos = Game.PlayerPed.Position;
                    TriggerServerEvent("uGamers:SaveAccount", pos.X, pos.Y, pos.Z);
                }
                _firstTick = true;
            }

            await Delay(30000); // 30 segundos
        }
    }
}
