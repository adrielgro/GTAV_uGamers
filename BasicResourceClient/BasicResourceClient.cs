﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using System.Dynamic;
using System.Collections.Generic;
using BasicResourceClient.NUI;

namespace BasicResourceClient
{
    public class BasicResourceClient : BaseScript
    {
        private bool _firstTick = false;

        public BasicResourceClient()
        {
            EventHandlers["registerUI"] += new Action(RegisterUI);
            EventHandlers["onClientResourceStart"] += new Action<string>((string obj) =>
            {
                if (obj == Function.Call<string>(Hash.GET_CURRENT_RESOURCE_NAME))
                {
                    //await Delay(500);
                    try
                    {
                        //SetupConnections();
                        Register.InitData();
                        Register.DisableUI();
                    }
                    catch (Exception e)
                    {
                        TriggerServerEvent($"ONDEBUG", e.ToString());
                        Screen.ShowNotification($"ERROR:{e.Message}");
                        Screen.ShowNotification($"ERROR:{e.StackTrace}");
                        Tick -= OnTick;
                        throw;
                    }
                }
            });

            API.SetCanAttackFriendly(API.GetPlayerPed(-1), true, false);
            API.NetworkSetFriendlyFireOption(true);

            API.DisplayCash(true); // True = Mostrar siempre el dinero(disables cash amount rendering), False = Mostrarlo cuando sea necesario
            API.RemoveMultiplayerHudCash();
            API.RemoveMultiplayerBankCash();

            API.SetPoliceIgnorePlayer(API.GetPlayerPed(-1), true); // Ignorado por los policias de GTA V
            API.SetMaxWantedLevel(0);

            Function.Call((Hash)0x170F541E1CADD1DE, false); // Related to displaying cash on the HUD
                                                            //Function.Call(Hash.DISPLAY_RADAR, false);

            /*
             * * Blips
             */
            var TownHallBlip = API.AddBlipForCoord(-139.3458f, 6201.6570f, 31.2122f); // Ayuntamiento
            API.SetBlipSprite(TownHallBlip, 409);
            API.SetBlipScale(TownHallBlip, 1f);
            API.SetBlipAsShortRange(TownHallBlip, true);

            var BankBlip = API.AddBlipForCoord(-111.9484f, 6461.6370f, 31.4895f);
            API.SetBlipSprite(BankBlip, 108);
            API.SetBlipScale(BankBlip, 1f);
            API.SetBlipAsShortRange(BankBlip, true);
            API.SetBlipColour(BankBlip, 2);

            var MechanicJobBlip = API.AddBlipForCoord(107.0220f, 6612.3560f, 31.9810f);
            API.SetBlipSprite(MechanicJobBlip, 446);
            API.SetBlipScale(MechanicJobBlip, 1f);
            API.SetBlipAsShortRange(MechanicJobBlip, true);

            var PostmanJobBlip = API.AddBlipForCoord(-421.1296f, 6136.662f, 31.8773f);
            API.SetBlipSprite(PostmanJobBlip, 89);
            API.SetBlipScale(PostmanJobBlip, 1f);
            API.SetBlipAsShortRange(PostmanJobBlip, true);

            var TruckerJobBlip = API.AddBlipForCoord(-17.8192f, 6304.3840f, 31.3749f);
            API.SetBlipSprite(TruckerJobBlip, 67);
            API.SetBlipScale(TruckerJobBlip, 1f);
            API.SetBlipAsShortRange(TruckerJobBlip, true);

            var FarmerJobBlip = API.AddBlipForCoord(413.0002f, 6539.6220f, 27.7272f);
            API.SetBlipSprite(FarmerJobBlip, 501);
            API.SetBlipScale(FarmerJobBlip, 1f);
            API.SetBlipAsShortRange(FarmerJobBlip, true);

            var CarrierJobBlip = API.AddBlipForCoord(36.8595f, 6549.8250f, 31.4255f);
            API.SetBlipSprite(CarrierJobBlip, 479);
            API.SetBlipScale(CarrierJobBlip, 1f);
            API.SetBlipAsShortRange(CarrierJobBlip, true);

            var FoodDeliveryManJobBlip = API.AddBlipForCoord(-134.8040f, 6378.8970f, 31.7539f);
            API.SetBlipSprite(FoodDeliveryManJobBlip, 93);
            API.SetBlipScale(FoodDeliveryManJobBlip, 1f);
            API.SetBlipAsShortRange(FoodDeliveryManJobBlip, true);

            var GarbageManJobBlip = API.AddBlipForCoord(-85.9159f, 6493.6570f, 31.4909f);
            API.SetBlipSprite(GarbageManJobBlip, 318);
            API.SetBlipScale(GarbageManJobBlip, 1f);
            API.SetBlipAsShortRange(GarbageManJobBlip, true);

            var TaxiDriverJobBlip = API.AddBlipForCoord(-38.3101f, 6419.4450f, 31.4904f);
            API.SetBlipSprite(TaxiDriverJobBlip, 56);
            API.SetBlipScale(TaxiDriverJobBlip, 1f);
            API.SetBlipAsShortRange(TaxiDriverJobBlip, true);

            Tick += OnTick;
        }
        

        private void RegisterUI()
        {
            Register.EnableUI();
            //Screen.ShowNotification($"¡Gracias por registrarte!");
        }

        /*private void SetupConnections()
        {
            API.RegisterCommand("elsui", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                if (arguments.Count != 1) return;
                if (arguments[0].Equals("enable"))
                {
                    Register.EnableUI();
                }
                else if (arguments[0].Equals("disable"))
                {
                    Register.DisableUI();
                }
                else if (arguments[0].Equals("show"))
                {
                    Register.ShowUI();
                }
            }), false);
        }*/

        #region Callbacks for GUI
        public void RegisterNUICallback(string msg, Func<IDictionary<string, object>, CallbackDelegate, CallbackDelegate> callback)
        {
            //Debug.WriteLine($"Registering NUI EventHandler for {msg}");
            API.RegisterNuiCallbackType(msg);

            EventHandlers[$"__cfx_nui:{msg}"] += new Action<ExpandoObject, CallbackDelegate>((body, resultCallback) =>
            {
                //Console.WriteLine("We has event" + body);
                callback.Invoke(body, resultCallback);

                string[] playerData = new string[6];
                int i = 0;
                foreach (KeyValuePair<string, Object> d in body)
                {
                    playerData[i++] = d.Value.ToString();
                }
                TriggerServerEvent("uGamers:CreateAccount", playerData[0], playerData[1], playerData[2], playerData[3], playerData[4], playerData[5]); // Llamar funcion del servidor
            });
            EventHandlers[$"{msg}"] += new Action<ExpandoObject, CallbackDelegate>((body, resultCallback) =>
            {
                //Console.WriteLine("We has event without __cfx_nui" + body);
                callback.Invoke(body, resultCallback);
            });
        }
        #endregion

        private async Task OnTick()
        {
            try
            {
                if (!_firstTick)
                {
                    RegisterNUICallback("finish", Register.FinishUI);
                    _firstTick = true;
                }
            }
            catch (Exception ex)
            {
                //TriggerServerEvent($"ONDEBUG", ex.ToString());
                //await Delay(5000);
                Screen.ShowNotification($"ERROR {ex}", true);
                //throw ex;
            }
            //Function.Call(Hash.CLEAR_PLAYER_WANTED_LEVEL, LocalPlayer);
            //await Delay(1000); // Cada segundo se ejecuta

            API.HideHudComponentThisFrame(1); // – Wanted Stars
            API.HideHudComponentThisFrame(2); // – Weapon Icon
            API.HideHudComponentThisFrame(3); // – Cash
            API.HideHudComponentThisFrame(4); // – MP Cash
            API.HideHudComponentThisFrame(6); // – Vehicle Name
            API.HideHudComponentThisFrame(7); // – Area Name
            API.HideHudComponentThisFrame(8); // – Vehicle Class
            API.HideHudComponentThisFrame(9); // – Street Name
            API.HideHudComponentThisFrame(13); // – Cash Change
            API.HideHudComponentThisFrame(17); // – Save Game
            API.HideHudComponentThisFrame(20); // – Weapon Stats

            await Task.FromResult(0);
        }

    }
}