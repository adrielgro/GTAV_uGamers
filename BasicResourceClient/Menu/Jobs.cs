﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using NativeUI;

namespace BasicResourceClient.Menu
{
    class Jobs : BaseScript
    {
        public UIMenu menu;

        public Jobs()
        {
            menu = new UIMenu("Lista de empleos", "");

            AddTruckerItem(menu);
            AddFarmerItem(menu);
            AddMechanicItem(menu);
            AddGarbageManItem(menu);
            AddPostmanItem(menu);
            AddFoodDeliveryItem(menu);
            AddTaxiDriverItem(menu);
            AddCarrierItem(menu);

            menu.RefreshIndex();
            menu.MouseControlsEnabled = false;
        }

        private void AddTruckerItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Camionero");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                API.SetNewWaypoint(-17.8192f, 6304.3840f);
            };
        }

        

        private void AddFarmerItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Granjero");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                API.SetNewWaypoint(413.0002f, 6539.6220f);
            };
        }

        private void AddMechanicItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Mecánico");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                if (item == newItem)
                {
                    //string output = "Ah perro traes el {0}";
                    //Screen.ShowSubtitle(String.Format(output, "omnitrix"));
                    API.SetNewWaypoint(107.0220f, 6612.3560f);
                }
            };
        }

        private void AddGarbageManItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Recolector de basura");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                API.SetNewWaypoint(-85.9159f, 6493.6570f);
            };
        }

        private void AddPostmanItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Repartidor de correos");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                API.SetNewWaypoint(-421.1296f, 6136.662f);
            };
        }

        private void AddFoodDeliveryItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Repartidor de comida");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
               API.SetNewWaypoint(-134.8040f, 6378.8970f);
            };
        }

        private void AddTaxiDriverItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Taxista");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                API.SetNewWaypoint(-38.3101f, 6419.4450f);
            };
        }

        private void AddCarrierItem(UIMenu menu)
        {
            var newItem = new UIMenuItem("Transportista");
            menu.AddItem(newItem);

            menu.OnItemSelect += (sender, item, index) =>
            {
                API.SetNewWaypoint(36.8595f, 6549.8250f);
            };
        }
    }
}
