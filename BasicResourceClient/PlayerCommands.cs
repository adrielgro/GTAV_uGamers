﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;

namespace BasicResourceClient
{
    class PlayerCommands : BaseScript
    {
        public PlayerCommands()
        {
            /* Comandos developers */
            API.RegisterCommand("getpos", new Action(DevGetPlayerPos), false);
            LoadAdminCommands();
            LoadUserCommands();
        }

        /*====================================================[ADMIN]====================================================*/
        private bool IsAdmin()
        {
            Ped player = Game.PlayerPed;
            if (Decorator.HasDecor(player, "Player.Admin"))
                if (Decorator.GetDecor<bool>(player, "Player.Admin"))
                    return true;
                else
                    return false;
            else
                return false;
        }

        private void LoadAdminCommands()
        {
            API.RegisterCommand("aget", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                if (!IsAdmin())
                {
                    TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1No tienes permisos de Administrador.");
                    return;
                }

                if (arguments.Count > 0)
                {
                    if (int.TryParse(arguments.First().ToString(), out int targetID))
                    {
                        try
                        {
                            int target = API.GetPlayerPed(targetID + 1);
                            API.SetEntityCoords(target, Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y, Game.PlayerPed.Position.Z, true, false, false, true);
                        }
                        catch (Exception)
                        {
                            TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1El ID del jugador es incorrecto.");
                        }
                    }
                }
            }), false);

            API.RegisterCommand("agoto", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                if (!IsAdmin())
                {
                    TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1No tienes permisos de Administrador.");
                    return;
                }

                if (arguments.Count > 0)
                {
                    if (int.TryParse(arguments.First().ToString(), out int targetID))
                    {
                        try
                        {
                            int player = API.GetPlayerPed(-1);

                            PlayerList pl = new PlayerList();
                            Player target = pl[targetID];

                            API.SetEntityCoords(player, target.Character.Position.X, target.Character.Position.Y, target.Character.Position.Z, true, false, false, true);
                        }
                        catch (Exception)
                        {
                            TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1El ID del jugador es incorrecto.");
                        }
                    }
                }
            }), false);

            API.RegisterCommand("atp", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                if (!IsAdmin())
                {
                    TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1No tienes permisos de Administrador.");
                    return;
                }

                if (arguments.Count > 0)
                {
                    Vector3 paleto = new Vector3(-139.3458f, 6201.6570f, 31.2122f);
                    string place = arguments.ElementAt(0).ToString();

                    if (place == "paleto")
                        API.SetEntityCoords(API.GetPlayerPed(-1), paleto.X, paleto.Y, paleto.Z, true, false, false, true);
                    else
                        TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^0Utiliza: ^5/atp [paleto]");
                }
            }), false);

            API.RegisterCommand("aveh", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                if (!IsAdmin())
                {
                    TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1No tienes permisos de Administrador.");
                    return;

                }
                if (arguments.Count > 0)
                {
                    Ped player = Game.PlayerPed;
                    string modelName = arguments.ElementAt(0).ToString();

                    uint modelHash = (uint)API.GetHashKey(modelName);

                    if (API.IsModelInCdimage(modelHash))
                    {
                        API.RequestModel(modelHash);

                        var veh = API.CreateVehicle(modelHash, player.Position.X, player.Position.Y, player.Position.Z + 1f, API.GetEntityHeading(API.GetPlayerPed(-1)), true, false);

                        Vehicle vehicle = new Vehicle(veh)
                        {
                            NeedsToBeHotwired = false,
                            IsEngineRunning = true,
                        };

                        Decorator.SetDecor(vehicle, "Vehicle.Admin", true);
                        //Decorator.SetDecor(vehicle, "Vehicle.OwnerID", API.GetPlayerServerId(-1));

                        new Ped(API.PlayerPedId()).SetIntoVehicle(vehicle, VehicleSeat.Driver);
                        API.SetVehicleOnGroundProperly(vehicle.Handle);
                    }
                    else
                    {
                        TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1El nombre del vehículo es incorrecto.");
                    }
                }
            }), false);

            API.RegisterCommand("asaveveh", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                if (!IsAdmin())
                {
                    TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1No tienes permisos de Administrador.");
                    return;
                }

                if (API.IsPedInAnyVehicle(API.GetPlayerPed(-1), false))
                {
                    // TYPE:
                    //   TAXI
                    //   
                    var veh = API.GetVehiclePedIsIn(API.GetPlayerPed(-1), false);
                    var modelClass = API.GetEntityModel(veh);
                    var modelName = API.GetDisplayNameFromVehicleModel((uint)modelClass);

                    Vehicle vehicle = new Vehicle(veh);
                    if (Decorator.HasDecor(vehicle, "Vehicle.Id"))
                        TriggerServerEvent("uGamers:updateVehicle", Decorator.GetDecor<bool>(vehicle, "Vehicle.Id"), modelName, "TAXI", vehicle.Position.X, vehicle.Position.Y, vehicle.Position.Z, vehicle.Rotation.X, vehicle.Rotation.Y, vehicle.Rotation.Z);
                    else
                        TriggerServerEvent("uGamers:saveVehicle", modelName, "TAXI", vehicle.Position.X, vehicle.Position.Y, vehicle.Position.Z, vehicle.Rotation.X, vehicle.Rotation.Y, vehicle.Rotation.Z);
                }
            }), false);

            API.RegisterCommand("asetadmin", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                if (!IsAdmin())
                {
                    TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1No tienes permisos de Administrador.");
                    return;
                }

                if (arguments.Count > 0)
                {
                    if (int.TryParse(arguments.First().ToString(), out int targetID))
                    {
                        try
                        {
                            Ped target = new Ped(targetID);

                            if (arguments.ElementAt(1).ToString() == "1337")
                                Decorator.SetDecor(target, "Player.Admin", true);
                            else
                                Decorator.SetDecor(target, "Player.Admin", false);
                        }
                        catch (Exception)
                        {
                            TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^1El ID del jugador es incorrecto.");
                        }
                    }
                    else
                    {
                        TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^0Utiliza: ^5/aSetAdmin [ID del jugador] [Nivel]");
                    }
                }
                else
                {
                    TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^0Utiliza: ^5/aSetAdmin [ID del jugador] [Nivel]");
                }
            }), false);

        }

        /*===================================================[DEVELOPER]===================================================*/
        public void DevGetPlayerPos()
        {
            Vector3 pos = LocalPlayer.Character.Position;
            Debug.WriteLine("X: " + pos.X + ", Y: " + pos.Y + ", Z: " + pos.Z);
        }
        

        /*=====================================================[USER]=====================================================*/
        private void LoadUserCommands()
        {
            API.RegisterCommand("ayuda", new Action<int, List<object>, string>((source, arguments, raw) =>
            {
                TriggerEvent("chatMessage", "", new[] { 255, 255, 255 }, "^0Comandos de ayuda...");
            }), false);
        }

    }
}
