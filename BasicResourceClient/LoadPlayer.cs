﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;

namespace BasicResourceClient
{
    class LoadPlayer : BaseScript
    {
        private bool _firstSpawn = false;

        public int Id { get; private set; }
        public String Name { get; private set; }
        public String Lastname { get; private set; }

        public LoadPlayer() {
            EventHandlers.Add("playerSpawned", new Action<Vector3>(OnPlayerSpawned));
            EventHandlers.Add("loadNewPlayer", new Action<int, string, string>(LoadNewPlayer));
            EventHandlers.Add("loadPlayerData", new Action<int, string, string, int, int, int, int, int, int, float, float, float>(LoadPlayerData));
            EventHandlers.Add("reLoadPlayerData", new Action<int, string, string, int, int, int, int, int, int>(ReloadPlayerData));

            Tick += OnTextDrawPlayer;
        }

        private async Task OnTextDrawPlayer()
        {
            API.SetTextFont(4);
            API.SetTextProportional(false);
            API.SetTextScale(0.5f, 0.5f);
            API.SetTextColour(255, 255, 255, 255);
            API.SetTextDropshadow(0, 0, 0, 0, 255);
            API.SetTextEdge(1, 0, 0, 0, 255);
            API.SetTextDropShadow();
            API.SetTextOutline();
            API.SetTextEntry("STRING");
            API.AddTextComponentString(Name + " " + Lastname);
            API.DrawText(0.016f, 0.77f);

            await Task.FromResult(0);
        }

        private void OnPlayerSpawned([FromSource]Vector3 pos)
        {
            if (!_firstSpawn)
            {
                TriggerServerEvent("uGamers:ExistAccount");
                _firstSpawn = true;
            }
            else
            { // Ocurre cuando muere
                TriggerEvent("chatMessage", "", new[] { 0, 0, 255 }, "^1Tuviste un accidente y has sido trasladado al hospital, la operación te ha costado...");
                API.SetEntityCoords(API.PlayerPedId(), -384.1744f, 6122.6480f, 31.4795f, true, false, false, true);
                
            }
        }

        private void LoadNewPlayer(int id, string name, string lastname)
        {
            int playerId = API.GetPlayerServerId(-1)+1;
            Ped player = Game.PlayerPed;

            TriggerEvent("chatMessage", "", new[] { 0, 0, 255 }, "^0¡Bienvenido a ^1uGamers ^4R^0oleplay ^4G^0aming!");
            API.SetEntityCoords(API.PlayerPedId(), -139.3458f, 6201.6570f, 31.2122f, true, false, false, true);
            //API.CreateMpGamerTag(API.GetPlayerPed(-1), "[" + playerId + "] " + name, true, false, "", 1);

            this.Name = name;
            this.Lastname = lastname;

            Decorator.SetDecor(player, "Player.Id", id);
            Decorator.SetDecor(player, "Player.Level", 1);
            Decorator.SetDecor(player, "Player.Exp", 0);
            Decorator.SetDecor(player, "Player.Health", 100);
            Decorator.SetDecor(player, "Player.Money", 2500);
            Decorator.SetDecor(player, "Player.Bank", 0);
            Decorator.SetDecor(player, "Player.Admin", false);
            
            AccountManager._logged = true;
        }

        private void LoadPlayerData(int id, string name, string lastname, int level, int exp, int health, int money, int bank, int admin, float pos_x, float pos_y, float pos_z)
        {
            Ped player = Game.PlayerPed;

            TriggerEvent("chatMessage", "", new[] { 0, 0, 255 }, "^0¡Bienvenido nuevamente a ^1uGamers ^4R^0oleplay ^4G^0aming!");
            API.SetEntityCoords(API.PlayerPedId(), pos_x, pos_y, pos_z, true, false, false, true);
            //API.CreateMpGamerTag(API.GetPlayerPed(-1), "[" + playerId + "] " + name, true, false, "", 1);

            this.Name = name;
            this.Lastname = lastname;

            Decorator.SetDecor(player, "Player.Id", id);
            Decorator.SetDecor(player, "Player.Level", level);
            Decorator.SetDecor(player, "Player.Exp", exp);
            Decorator.SetDecor(player, "Player.Health", health);
            Decorator.SetDecor(player, "Player.Money", money);
            Decorator.SetDecor(player, "Player.Bank", bank);
            if(admin == 1337)
                Decorator.SetDecor(player, "Player.Admin", true);
            else
                Decorator.SetDecor(player, "Player.Admin", false);

            AccountManager._logged = true;
        }

        private void ReloadPlayerData(int id, string name, string lastname, int level, int exp, int health, int money, int bank, int admin)
        {
            if (_firstSpawn)
            {
                Ped player = Game.PlayerPed;

                this.Name = name;
                this.Lastname = lastname;

                Decorator.SetDecor(player, "Player.Id", id);
                Decorator.SetDecor(player, "Player.Level", level);
                Decorator.SetDecor(player, "Player.Exp", exp);
                Decorator.SetDecor(player, "Player.Health", health);
                Decorator.SetDecor(player, "Player.Money", money);
                Decorator.SetDecor(player, "Player.Bank", bank);
                if (admin == 1337)
                    Decorator.SetDecor(player, "Player.Admin", true);
                else
                    Decorator.SetDecor(player, "Player.Admin", false);
            }
        }

    }
}
