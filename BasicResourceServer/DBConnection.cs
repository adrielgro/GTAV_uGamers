﻿using System;
using System.Collections.Generic;
using CitizenFX.Core;
using MySql.Data.MySqlClient;

namespace BasicResourceServer
{
    class DBConnection : BaseScript
    {
        private const String SERVER = "51.38.21.65";
        private const String DATABASE = "gtav_rpg";
        private const String UID = "gtav";
        private const String PASSWORD = "GJgp8sRvMaTzc4BG8F";
        private static MySqlConnection dbConn;

        public int Id { get; private set; }
        public String Username { get; private set; }
        public String Password { get; private set; }

        public DBConnection()
        {
            
        }

        private DBConnection(int id, String u, String p)
        {
            Id = id;
            Username = u;
            Password = p;
        }

        public static void InitializeDB()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder
            {
                Server = SERVER,
                UserID = UID,
                Password = PASSWORD,
                Database = DATABASE
            };

            String connString = builder.ToString();
            builder = null;

             //Console.WriteLine(connString); // Datos de la conexion

            dbConn = new MySqlConnection(connString);
        }

        /*public static List<DBConnection> GetUsers()
        {
            List<DBConnection> users = new List<DBConnection>();
            String query = "SELECT * FROM users";

            MySqlCommand cmd = new MySqlCommand(query, dbConn);
            dbConn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = (int)reader["id"];
                String username = reader["username"].ToString();
                String password = reader["password"].ToString();

                DBConnection u = new DBConnection(id, username, password);

                users.Add(u);
            }

            reader.Close();
            dbConn.Close();
            return users;
        }*/

        public static DBConnection Insert(String u, String p)
        {
            String query = string.Format("INSERT INTO users(username, password) VALUES ('{0}', '{1}')", u, p);
            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();
            cmd.ExecuteNonQuery();

            int id = (int)cmd.LastInsertedId;

            DBConnection user = new DBConnection(id, u, p);

            dbConn.Close();
            return user;
        }

        public void Update(string u, string p)
        {
            String query = string.Format("UPDATE users SET username='{0}', password='{1}' WHERE ID={2}", u, p, Id);

            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();
            cmd.ExecuteNonQuery();

            dbConn.Close();
        }

        /*public void Delete()
        {
            String query = string.Format("DELETE FROM users WHERE ID={0}", Id);
            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();
            cmd.ExecuteNonQuery();
            dbConn.Close();
        }*/

        public int CreateAccount(string identifier, string email, string password, string name, string lastname, string age, string sex)
        {
            String query = string.Format("INSERT INTO users(identifier, email, password, name, lastname, age, sex) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", 
                identifier, email, password, name, lastname, age, sex);
            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();

            try
            {
                cmd.ExecuteNonQuery();
                int id = (int)cmd.LastInsertedId;
                dbConn.Close();

                return id;
            }
            catch (Exception e) {
                Debug.WriteLine("Error: " + e.ToString());
                return 0;
            } 
        }

        public List<string> GetDataAccount(string identifier)
        {
            var dataPlayer = new List<string>();

            String query = string.Format("SELECT * FROM users WHERE identifier='{0}'", identifier);
            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                dataPlayer.Add(reader["id"].ToString());
                dataPlayer.Add(reader["name"].ToString());
                dataPlayer.Add(reader["lastname"].ToString());
                dataPlayer.Add(reader["level"].ToString());
                dataPlayer.Add(reader["exp"].ToString());
                dataPlayer.Add(reader["health"].ToString());
                dataPlayer.Add(reader["money"].ToString());
                dataPlayer.Add(reader["bank"].ToString());
                dataPlayer.Add(reader["admin"].ToString());
                dataPlayer.Add(reader["pos_x"].ToString());
                dataPlayer.Add(reader["pos_y"].ToString());
                dataPlayer.Add(( float.Parse(reader["pos_z"].ToString()) + 0.5 ).ToString());
                break;
            }

            reader.Close();
            dbConn.Close();

            return dataPlayer;
        }

        public bool ExistAccount(string identifier)
        {
            String query = string.Format("SELECT COUNT(*) FROM users WHERE identifier='{0}'", identifier);
            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();
            int count = Convert.ToInt32(cmd.ExecuteScalar());
            dbConn.Close();

            if (count == 0)
                return false;
            else
                return true;
        }

        public void SaveAccount(string identifier, float posx, float posy, float posz)
        {
            String query = string.Format("UPDATE users SET pos_x='{0}', pos_y='{1}', pos_z='{2}' WHERE identifier='{3}'", posx, posy, posz, identifier);

            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();
            cmd.ExecuteNonQuery();

            dbConn.Close();
            //Console.WriteLine("[Save]Player");
        }

        public void SetAdmin(string identifier, int level)
        {
            String query = string.Format("UPDATE users SET admin={0} WHERE identifier='2'", level, identifier);

            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();
            cmd.ExecuteNonQuery();

            dbConn.Close();
        }

        //=======================================[VEHICLES]=======================================
        public int CreateVehicle(string modelname, string type, float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z)
        {
            String query = string.Format("INSERT INTO vehicles(model, type, pos_x, pos_y, pos_z, rot_x, rot_y, rot_z) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5}, {6}, {7})",
                modelname, type, pos_x, pos_y, pos_z, rot_x, rot_y, rot_z);
            MySqlCommand cmd = new MySqlCommand(query, dbConn);

            dbConn.Open();

            try
            {
                cmd.ExecuteNonQuery();
                int id = (int)cmd.LastInsertedId;
                dbConn.Close();

                return id;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e.ToString());
                dbConn.Close();
                return 0;
            }
        }
    }
}
