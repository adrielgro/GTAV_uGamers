﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace BasicResourceServer
{
    class LoadServer : BaseScript
    {
        public LoadServer()
        {
            API.SetMapName("Paleto Bay");
            API.SetGameType("Roleplay Gaming");
            
            DBConnection.InitializeDB(); // Inicializamos la conexion a la BD
        }
    }
}
