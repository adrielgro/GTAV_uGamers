﻿using System;
using System.Collections.Generic;
using System.Linq;
using CitizenFX.Core;

namespace BasicResourceServer
{
    public class BasicResourceServer : BaseScript
    {
        DBConnection dbConnection = new DBConnection();

        public BasicResourceServer()
        {
            EventHandlers.Add("onResourceStart", new Action<string>(OnResourceStart));

            EventHandlers.Add("playerConnecting", new Action<Player, string, CallbackDelegate>(OnPlayerConnecting));
            EventHandlers.Add("playerSpawned", new Action<Player>(OnPlayerSpawned));
            EventHandlers.Add("playerDropped", new Action<Player, string>(OnPlayerDisconnect));

            EventHandlers.Add("chatMessage", new Action<int, string, string>(OnPlayerText));
            
            EventHandlers.Add("baseevents:onPlayerKilled", new Action<Player, int>(OnPlayerKilled));
            EventHandlers.Add("baseevents:onPlayerDied", new Action<Player, string>(OnPlayerDied));

            EventHandlers.Add("baseevents:enteredVehicle", new Action<Player, object, int, string>(OnPlayerEnteredVehicle));
            EventHandlers.Add("baseevents:enteringVehicle", new Action<Player, int, int, string>(OnPlayerEnteringVehicle));
            EventHandlers.Add("baseevents:leftVehicle", new Action<Player, object, int, string>(OnPlayerLeftVehicle));

            EventHandlers.Add("uGamers:ExistAccount", new Action<Player>(OnExistAccount));
            EventHandlers.Add("uGamers:CreateAccount", new Action<Player, string, string, string, string, string, string>(OnCrateAccount));
            EventHandlers.Add("uGamers:SaveAccount", new Action<Player, float, float, float>(OnSaveAccount));

            EventHandlers.Add("uGamers:saveVehicle", new Action<string, string, float, float, float, float, float, float>(OnSaveVehicle));
        }

        private void OnSaveVehicle(string modelname, string type, float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z)
        {
            int id = dbConnection.CreateVehicle(modelname, type, pos_x, pos_y, pos_z, rot_x, rot_y, rot_z);
        }

        private void OnResourceStart(string resourceName)
        {
            Debug.WriteLine($"Resource was succesfully started {resourceName}");
        }

        private void OnPlayerConnecting([FromSource]Player player, string playerName, CallbackDelegate kickReason)
        {
            Debug.WriteLine($"{player.Name} ha entrado al servidor, IP: {player.EndPoint.ToString()}");
        }

        private void OnPlayerSpawned([FromSource]Player player)
        {
            string identifier = player.Identifiers.First();
            List<string> data = dbConnection.GetDataAccount(identifier);

            string id = data[0];
            string name = data[1];
            string lastname = data[2];
            string level = data[3];
            string exp = data[4];
            string health = data[5];
            string money = data[6];
            string bank = data[7];
            string admin = data[8];

            Console.WriteLine("OnPlayerSpawned: " +
                    "[ID]: " + id +
                    ", [Name]: " + name +
                    ", [Lastname]: " + lastname +
                    ", [Level]: " + level +
                    ", [Exp]: " + exp +
                    ", [Health]: " + health +
                    ", [Money]: " + money +
                    ", [Bank]: " + bank +
                    ", [Admin]: " + admin);

            TriggerClientEvent(player, "reloadPlayerData", id, name, lastname, level, exp, health, money, bank, admin);
        }

        public void OnPlayerDisconnect([FromSource]Player player, string reason)
        {
            Debug.WriteLine($"El jugador {player.Name} se ha desconectado (razon: {reason})");
        }

        private void OnExistAccount([FromSource]Player player)
        {
            string identifier = player.Identifiers.First();

            if (dbConnection.ExistAccount(identifier))
            { // Jugador registrado
                Console.WriteLine("El jugador esta registrado: " + identifier);
                List<string> data = dbConnection.GetDataAccount(identifier);

                string id = data[0];
                string name = data[1];
                string lastname = data[2];
                string level = data[3];
                string exp = data[4];
                string health = data[5];
                string money = data[6];
                string bank = data[7];
                string admin = data[8];
                string pos_x = data[9];
                string pos_y = data[10];
                string pos_z = data[11];

                

                TriggerClientEvent(player, "loadPlayerData", id, name, lastname, level, exp, health, money, bank, admin, pos_x, pos_y, pos_z);
            }
            else
            {
                Console.WriteLine("El jugador NO esta registrado: " + identifier);
                TriggerClientEvent(player, "registerUI"); // Cargar pantalla de registro
            }
        }

        private void OnCrateAccount([FromSource]Player player, string age, string email, string lastname, string name, string password, string sex)
        {
            string identifier = player.Identifiers.First();
            int id = dbConnection.CreateAccount(identifier, email, password, name, lastname, age, sex);

            TriggerClientEvent(player, "loadNewPlayer", id, name, lastname); // Cargar datos por defecto
        }

        private void OnSaveAccount([FromSource]Player player, float posX, float posY, float posZ)
        {
            string identifier = player.Identifiers.First();
            dbConnection.SaveAccount(identifier, posX, posY, posZ);
        }

        private void OnPlayerText([FromSource]int player, string playerName, string chatMessage)
        {
            
        }
        
        public void OnPlayerKilled([FromSource]Player victim, int killerID)
        {
            Player killer = Players[killerID];
            Debug.WriteLine($"El jugador {victim.Name} ha sido asesinado por {killer.Name}");
        }

        private void OnPlayerDied([FromSource]Player player, string deathReason)
        {
            Debug.WriteLine("OnPlayerDied! " + player.Name + " razon = " + deathReason);
        }

        private void OnPlayerEnteringVehicle([FromSource]Player player, int vehicleHash, int seat, string displayName)
        {
            //Debug.WriteLine($"El jugador {player.Name} esta entrando en un {displayName} {vehicleHash} en el asiento {seat}");
        }

        private void OnPlayerEnteredVehicle([FromSource]Player player, object vehicle, int seat, string displayName)
        {
            //Debug.WriteLine($"El jugador {player.Name} ha entrado a un {displayName} {vehicle} en el asiento {seat}");
        }

        private void OnPlayerLeftVehicle([FromSource]Player player, object vehicle, int seat, string displayName)
        {
            //Debug.WriteLine($"El jugador {player.Name} ha salido de un {displayName} {vehicle} en el asiento {seat}");
        }
        
    }
}
